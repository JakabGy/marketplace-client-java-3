package com.atlassian.marketplace.client.http;

import com.atlassian.marketplace.client.MpacException;

import java.io.Closeable;
import java.io.InputStream;

/**
 * A simple abstraction of an HTTP response.
 * @since 2.0.0
 */
public interface SimpleHttpResponse extends Closeable
{
    /**
     * The status code of the response.
     */
    int getStatus();
    
    /**
     * An input stream for the response body.
     */
    InputStream getContentStream() throws MpacException;
    
    /**
     * Returns all header values for the given header name.
     */
    Iterable<String> getHeader(String name);
    
    /**
     * True if the response has a zero-length body.
     */
    boolean isEmpty();
    
    /**
     * Ensures that the input stream is closed.
     */
    void close();
}